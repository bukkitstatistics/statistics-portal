<h2>Error 404</h2>
<div class="alert alert-error">
    The step <strong>(<?php echo $this->prepare('url'); ?>)</strong> you requested could not be found.
</div>